
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';
import 'firebase/compat/auth';

// Replace this with your own config details
const config = {
  apiKey: "AIzaSyCdsmXo1LJF8xTCQtXLdsiVnKAIbNRG3ys",
  authDomain: "react-redux-firebase-dce6b.firebaseapp.com",
  projectId: "react-redux-firebase-dce6b",
  storageBucket: "react-redux-firebase-dce6b.appspot.com",
  messagingSenderId: "299406811968",
  appId: "1:299406811968:web:01d199df762396de5f90a5",
  measurementId: "G-VW1GSMLXPH"
};
firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase 