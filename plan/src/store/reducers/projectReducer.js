const initState = {
  projects: [
    {id: '1', title: 'Coffee caramels', content: 'Coffee caramels chocolate cake lemon drops sweet roll chupa chups pudding. Halvah carrot cake toffee cookie caramels soufflé sugar plum jujubes. Oat cake chocolate bar wafer chocolate bar lollipop toffee muffin carrot cake.'},
    {id: '2', title: 'Jelly jujubes', content: 'Jelly jujubes carrot cake dessert sweet roll cake marzipan soufflé candy. Cotton candy danish tootsie roll gummies apple pie. Cake cotton candy fruitcake biscuit chocolate bear claw croissant ice cream chocolate bar. Chupa chups icing lemon drops sesame snaps candy canes danish donut. Jelly beans croissant candy canes sugar plum pudding. '},
    {id: '3', title: 'Chocolate cake', content: 'Powder lollipop pudding macaroon chocolate cake tart. Halvah pastry jelly danish powder. Chocolate bar shortbread carrot cake candy liquorice jelly beans muffin danish soufflé. Muffin bear claw chocolate cake'}
  ]
}

const projectReducer = (state = initState, action) => {
  switch (action.type){
    case 'CREATE_PROJECT':
      console.log('action', action.project)
      return state;
    case 'CREATE_PROJECT_ERROR':
      console.log('create project err', action.err)
    return state;

    default:
      return state;
  }
};

export default projectReducer;