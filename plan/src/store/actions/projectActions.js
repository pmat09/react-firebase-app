

// export const createProject = (project) => {
//   return {
//     type:'CREATE_PROJECT',
//     project:project
//   }
// }

export const createProject = (project) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {

    const firestore = getFirestore();
    console.log('firestore', firestore.collection('projects'))
    firestore.collection('projects').add({
      ...project,
      authorFirstName:'Pau',
      authorLastName:'Mat',
      authorId:123,
      createdAt:new Date()
    }).then(() => {
      dispatch({type: 'CREATE_PROJECT', project: project})
    }).catch((err) => {
      console.log(err)
      dispatch({type: 'CREATE_PROJECT_ERROR', err})

    })
  }
};